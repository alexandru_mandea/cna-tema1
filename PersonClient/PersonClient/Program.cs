﻿using Grpc.Net.Client;
using PersonService.Protos;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PersonClient
{
    class Program
    {
        private static string ValidateId(string id)
        {
            Regex idPattern = new Regex("[1256][0-9]{2}(0[0-9]|10|11|12)(0[0-9]|1[0-9]|2[0-9]|30|31)[0-9]{6}");
            MatchCollection matches = idPattern.Matches(id);

            while(matches.Count==0)
            {
                Console.WriteLine("Incorrect id format. Type another one: ");
                id=Console.ReadLine();
                matches = idPattern.Matches(id);
            }
            return id;
        }

        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new PersonalDataCollector.PersonalDataCollectorClient(channel);

            var input = new DataRequest();
            var id = "";

            Console.WriteLine($"Write your name:");
            input.Name = Console.ReadLine();
            Console.WriteLine($"Write your id:");
            id = Console.ReadLine();
            input.Id = ValidateId(id);

            var proccess = await client.ShowInformationAsync(input);
            var response = await client.RecieveDataAsync(input);

            Console.WriteLine(response);

            Console.ReadLine();
        }
    }
}
