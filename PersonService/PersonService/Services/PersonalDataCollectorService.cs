﻿using Grpc.Core;
using PersonService.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonService.Services
{
    public class PersonalDataCollectorService : PersonalDataCollector.PersonalDataCollectorBase
    {
        private string GetGender(string id)
        {
            if (id[0] == '1' || id[0] == '5')
            {
                return "Male";
            }
            else
            if (id[0] == '2' || id[0] == '6')
            {
                return "Female";
            }
            return "Unknown";
        }

        private int CalculateAge(string id)
        {
            int age = 0;
            int year = 0;
            int month = Int32.Parse(id.Substring(3, 2));
            int day = Int32.Parse(id.Substring(5, 2));

            if (id[0] == '1' || id[0] == '2')
            {
                year = Int32.Parse(id.Substring(1, 2)) + 1900;
            }
            else
            if (id[0] == '5' || id[0] == '6')
            {
                year = Int32.Parse(id.Substring(1, 2)) + 2000;
            }

            var today = DateTime.Today;

            var a = (today.Year * 100 + today.Month) * 100 + today.Day;
            var b = (year * 100 + month) * 100 + day;

            age = (a - b) / 10000;

            return age;
        }

        public override Task<DataRequestResponse> RecieveData(DataRequest dataRequest, ServerCallContext context)
        {
            return Task.FromResult(new DataRequestResponse
            {
                Response = dataRequest.Name + ": data recieved and saved!"
            });
        }

        public override Task<PersonalInformation> ShowInformation(DataRequest dataRequest, ServerCallContext context)
        {
            var name = dataRequest.Name;
            string id = dataRequest.Id;

            Console.WriteLine();

            PersonalInformation personalInformation = new PersonalInformation { Name = name, Gender = GetGender(id), Age = CalculateAge(id) };

            Console.WriteLine($"Name: " + personalInformation.Name + ", Gender: " + personalInformation.Gender + ", age: " + personalInformation.Age);

            Console.WriteLine();

            return Task.FromResult(personalInformation);
        }
    }
}
